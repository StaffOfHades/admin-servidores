rm -f out
mkfifo out
trap "rm -f out" EXIT
while true
do
  cat out | nc -l ${1-8080} > >(
    # parse the netcat output, to build the answer redirected to the pipe "out".
    export REQUEST=
    while read line
    do
      line=$(echo "$line" | tr -d '[\r\n]')

      if echo "$line" | grep -qE '^GET /' # if line starts with "GET /"
      then
        REQUEST=$(echo "$line" | cut -d ' ' -f2) # extract the request
      elif [ "x$line" = x ] # empty line / end of request
      then
        HTTP_200="HTTP/1.1 200 OK"
        HTTP_LOCATION="Location:"
        HTTP_404="HTTP/1.1 404 Not Found"
        # call a script here
        # Note: REQUEST is exported, so the script can parse it (to answer 200/403/404 status code + content)
        if echo $REQUEST | grep -qE '^/time'
        then
            FILE="webpage.html"
            WEB_PAGE=`cat $FILE`
                FILE_LENGTH=`wc -c < $FILE`
            CONTENT_TYPE="Content-Type: text/html; charset=UTF-8"
            CONTENT_LENGTH="Content-Length: $FILE_LENGTH"
            printf "%s\n%s\n%s\n\n%s\n" "$HTTP_200" "$CONTENT_TYPE" "$FILE_LENGTH" "$WEB_PAGE" > out
        else
            printf "%s\n%s %s\n\n%s\n" "$HTTP_404" "$HTTP_LOCATION" $REQUEST "Resource $REQUEST NOT FOUND!" > out
        fi
      fi
    done
  )
done
