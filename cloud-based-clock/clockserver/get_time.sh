# !/bin/bash

{ echo -ne "HTTP/1.0 200 OK\r\nContent-Type: application/json\r\nContent-Length: $(wc -c < $1)\r\n\r\n"; cat $1; } | nc -k -l -p ${2-8080}
