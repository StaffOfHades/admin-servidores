val ScalatraVersion = "2.6.4"

organization := "tech.inferis"

name := "User Authentication"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.12.6"

resolvers += Classpaths.typesafeReleases

libraryDependencies ++= Seq(
	"org.scalatra" %% "scalatra" % ScalatraVersion,
	"org.scalatra" %% "scalatra-forms" % ScalatraVersion,
	"org.scalatra" %% "scalatra-scalatest" % ScalatraVersion % "test",
	"org.scalactic" %% "scalactic" % "3.0.5",
	"org.scalatest" %% "scalatest" % "3.0.5" % "test",
	"ch.qos.logback" % "logback-classic" % "1.2.3" % "runtime",
	"org.eclipse.jetty" % "jetty-webapp" % "9.4.9.v20180320" % "container",
	"javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
	"net.debasishg" %% "redisclient" % "3.9",
	"com.github.fppt" % "jedis-mock" % "0.1.10",
	"com.pauldijou" %% "jwt-json4s-native" % "2.1.0",
	"org.json4s" %% "json4s-native" % "3.6.5"
)

enablePlugins(SbtTwirl)
enablePlugins(ScalatraPlugin)

//logBuffered in Test := false
//testOptions in Test := Tests.Argument(TestFramework.ScalaTest, "-h", test-results")
