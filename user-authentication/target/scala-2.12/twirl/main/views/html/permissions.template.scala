
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml

object permissions extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(permission: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.22*/("""
"""),_display_(/*2.2*/layouts/*2.9*/.html.default("User Authentitacion: a simple, extensible, and most importantly, generic user authentacion service", "You are logged in!")/*2.146*/{_display_(Seq[Any](format.raw/*2.147*/("""
  """),format.raw/*3.3*/("""<p>You are viewing permission for """),_display_(/*3.38*/permission),format.raw/*3.48*/("""</p>
""")))}),format.raw/*4.2*/("""
"""))
      }
    }
  }

  def render(permission:String): play.twirl.api.HtmlFormat.Appendable = apply(permission)

  def f:((String) => play.twirl.api.HtmlFormat.Appendable) = (permission) => apply(permission)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Sat May 18 16:22:19 CDT 2019
                  SOURCE: /home/hades/Documents/UDLAP/Primavera 2019/Administracion de Redes y Servidores/Laboratorios/user-authentication/src/main/twirl/views/permissions.scala.html
                  HASH: e11c59ba2a3e7702905692784858482a4d11b6a0
                  MATRIX: 572->1|687->21|714->23|728->30|874->167|913->168|942->171|1003->206|1033->216|1068->222
                  LINES: 14->1|19->1|20->2|20->2|20->2|20->2|21->3|21->3|21->3|22->4
                  -- GENERATED --
              */
          