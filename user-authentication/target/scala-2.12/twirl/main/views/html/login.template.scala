
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml

object login extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[javax.servlet.http.HttpServletRequest,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/()(implicit request: javax.servlet.http.HttpServletRequest):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*2.2*/import org.scalatra.forms.views._


Seq[Any](format.raw/*1.61*/("""
"""),format.raw/*3.1*/("""
"""),_display_(/*4.2*/layouts/*4.9*/.html.default("User Authentitacion: Login", "Login for Access Credentials", Some("login.css"))/*4.103*/{_display_(Seq[Any](format.raw/*4.104*/("""
    """),format.raw/*5.5*/("""<form action="/login" method="POST">
        <label for="username">Usuario</label>
        <input type="text" id="username" name="username" placeholder="Usuario...">
        <span class="error">"""),_display_(/*8.30*/error("username")),format.raw/*8.47*/("""</span>
        <label for="password">Contraseña</label>
        <input type="password" id="password" name="password">
        <span class="error">"""),_display_(/*11.30*/error("password")),format.raw/*11.47*/("""</span>
        <input type="submit" value="Submit">
    </form>
""")))}),format.raw/*14.2*/("""
"""))
      }
    }
  }

  def render(request:javax.servlet.http.HttpServletRequest): play.twirl.api.HtmlFormat.Appendable = apply()(request)

  def f:(() => (javax.servlet.http.HttpServletRequest) => play.twirl.api.HtmlFormat.Appendable) = () => (request) => apply()(request)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Sat May 18 16:22:20 CDT 2019
                  SOURCE: /home/hades/Documents/UDLAP/Primavera 2019/Administracion de Redes y Servidores/Laboratorios/user-authentication/src/main/twirl/views/login.scala.html
                  HASH: aacf2b438e410cee0772b641607b4230133c523c
                  MATRIX: 597->1|729->62|792->60|819->96|846->98|860->105|963->199|1002->200|1033->205|1254->400|1291->417|1466->565|1504->582|1600->648
                  LINES: 14->1|17->2|20->1|21->3|22->4|22->4|22->4|22->4|23->5|26->8|26->8|29->11|29->11|32->14
                  -- GENERATED --
              */
          