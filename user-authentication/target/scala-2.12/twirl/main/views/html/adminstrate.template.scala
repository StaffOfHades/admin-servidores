
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml

object adminstrate extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(user: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.16*/("""
"""),_display_(/*2.2*/layouts/*2.9*/.html.default("User Authentitacion: a simple, extensible, and most importantly, generic user authentacion service", "You are logged in as Admin!")/*2.155*/{_display_(Seq[Any](format.raw/*2.156*/("""
  """),format.raw/*3.3*/("""<p>Welcome """),_display_(/*3.15*/user),format.raw/*3.19*/("""!</p>
  <p>Here you can do administration work</p>
""")))}),format.raw/*5.2*/("""
"""))
      }
    }
  }

  def render(user:String): play.twirl.api.HtmlFormat.Appendable = apply(user)

  def f:((String) => play.twirl.api.HtmlFormat.Appendable) = (user) => apply(user)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Sat May 18 16:22:20 CDT 2019
                  SOURCE: /home/hades/Documents/UDLAP/Primavera 2019/Administracion de Redes y Servidores/Laboratorios/user-authentication/src/main/twirl/views/adminstrate.scala.html
                  HASH: 7df450dfc0182c0dc6f10b91df88df185b9368c8
                  MATRIX: 572->1|681->15|708->17|722->24|877->170|916->171|945->174|983->186|1007->190|1088->242
                  LINES: 14->1|19->1|20->2|20->2|20->2|20->2|21->3|21->3|21->3|23->5
                  -- GENERATED --
              */
          