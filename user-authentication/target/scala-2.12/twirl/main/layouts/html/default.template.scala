
package layouts.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml

object default extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template5[String,String,Option[String],Option[String],Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String, headline: String, css: Option[String] = None, script: Option[String] = None)(body: Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.106*/("""


"""),format.raw/*4.1*/("""<html>
  <head>
    <title>"""),_display_(/*6.13*/title),format.raw/*6.18*/("""</title>
    """),_display_(/*7.6*/css/*7.9*/ match/*7.15*/ {/*8.9*/case Some(s) =>/*8.24*/ {_display_(Seq[Any](format.raw/*8.26*/("""
            """),_display_(/*9.14*/defining("/stylesheets/" + s)/*9.43*/{ style =>_display_(Seq[Any](format.raw/*9.53*/("""
                """),format.raw/*10.17*/("""<link rel="stylesheet" href="""),_display_(/*10.46*/style),format.raw/*10.51*/(""">
            """)))}),format.raw/*11.14*/("""
        """)))}/*13.9*/case None =>/*13.21*/ {}}),format.raw/*14.6*/("""
  """),format.raw/*15.3*/("""</head>
  <body>
    <div class="container">
        <div class="title">
            <h1>"""),_display_(/*19.18*/headline),format.raw/*19.26*/("""</h1>
            <hr>
        </div>
        <div class="box">
            """),_display_(/*23.14*/body),format.raw/*23.18*/("""
        """),format.raw/*24.9*/("""</div>
    </div>
    """),_display_(/*26.6*/script/*26.12*/ match/*26.18*/ {/*27.9*/case Some(s) =>/*27.24*/ {_display_(Seq[Any](format.raw/*27.26*/("""
            """),_display_(/*28.14*/defining("/scripts/" + s)/*28.39*/{ js =>_display_(Seq[Any](format.raw/*28.46*/("""
                """),format.raw/*29.17*/("""<script src="""),_display_(/*29.30*/js),format.raw/*29.32*/(""">
            """)))}),format.raw/*30.14*/("""
        """)))}/*32.9*/case None =>/*32.21*/ {}}),format.raw/*33.6*/("""
  """),format.raw/*34.3*/("""</body>
</html>
"""))
      }
    }
  }

  def render(title:String,headline:String,css:Option[String],script:Option[String],body:Html): play.twirl.api.HtmlFormat.Appendable = apply(title,headline,css,script)(body)

  def f:((String,String,Option[String],Option[String]) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title,headline,css,script) => (body) => apply(title,headline,css,script)(body)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Sat May 18 16:22:20 CDT 2019
                  SOURCE: /home/hades/Documents/UDLAP/Primavera 2019/Administracion de Redes y Servidores/Laboratorios/user-authentication/src/main/twirl/layouts/default.scala.html
                  HASH: 5129d0cbf2d792c10815bacb6a89aab6f353caf1
                  MATRIX: 612->1|812->105|841->108|895->136|920->141|959->155|969->158|983->164|992->175|1015->190|1054->192|1094->206|1131->235|1178->245|1223->262|1279->291|1305->296|1351->311|1379->330|1400->342|1424->351|1454->354|1571->444|1600->452|1704->529|1729->533|1765->542|1814->565|1829->571|1844->577|1854->588|1878->603|1918->605|1959->619|1993->644|2038->651|2083->668|2123->681|2146->683|2192->698|2220->717|2241->729|2265->738|2295->741
                  LINES: 14->1|19->1|22->4|24->6|24->6|25->7|25->7|25->7|25->8|25->8|25->8|26->9|26->9|26->9|27->10|27->10|27->10|28->11|29->13|29->13|29->14|30->15|34->19|34->19|38->23|38->23|39->24|41->26|41->26|41->26|41->27|41->27|41->27|42->28|42->28|42->28|43->29|43->29|43->29|44->30|45->32|45->32|45->33|46->34
                  -- GENERATED --
              */
          