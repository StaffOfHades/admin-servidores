package tech.inferis.userauth

import com.github.fppt.jedismock._

import com.redis.RedisClient

import org.json4s._
import org.json4s.JsonDSL.WithBigDecimal._
import org.json4s.native.JsonMethods._

import org.junit.runner.RunWith

import org.scalatest._
import org.scalatest.junit.JUnitRunner
import org.scalatra.test.scalatest._

import scala.util.{Try, Success, Failure}

import pdi.jwt._
import pdi.jwt.exceptions._

import tech.inferis.userauth.db._

@RunWith(classOf[JUnitRunner])
trait ExpandedFunSpec extends FunSpec with Matchers with BeforeAndAfterAll with BeforeAndAfterEach with GivenWhenThen
{
   implicit var parser: Parser = Base64Parser
}
trait ExpandedScalatraSpec extends ScalatraSpec with BeforeAndAfterEach with GivenWhenThen
{
   implicit var parser: Parser = Base64Parser
}

abstract class RedisTest extends ExpandedFunSpec {
    
   var redisServer: RedisServer = null
   var r: RedisClient = null
   implicit var db: DB = null

   override protected def beforeAll = {
      redisServer = RedisServer.newRedisServer;
      redisServer.start
      r = new RedisClient(redisServer.getHost, redisServer.getBindPort)
      db = new Redis(r)
   }

   override protected def afterAll = {
      r.disconnect
      redisServer.stop
   }

}

abstract class RedisServletTest extends ExpandedScalatraSpec {
    
   var redisServer: RedisServer = null
   var r: RedisClient = null
   implicit var db: DB = null

   override protected def beforeAll = {
      super.beforeAll
      redisServer = RedisServer.newRedisServer;
      redisServer.start
      r = new RedisClient(redisServer.getHost, redisServer.getBindPort)
      db = new Redis(r)
   }

   override protected def afterAll = {
      r.disconnect
      redisServer.stop
      super.afterAll
   }

}

//addServlet(classOf[HelloWorldServlet], "/*")

class JwtFuncionalityTest extends ExpandedFunSpec {
   
   describe("User session") { 
      describe("when using JWT tokens") {
         it("should generate a valid token") {
            Given("a claim containing the user & id")
            val claim = JObject(("id", 1), ("user", "hades"))
            
            When("the claim is encoded using a key to generate a token")
            val token = JwtJson4s.encode(claim, "secretKey", JwtAlgorithm.HS256)
            
            Then("the token should be able to be decodified using that key")
            val (isSuccess, result) = JwtJson4s.decodeJson(token, "secretKey", Seq(JwtAlgorithm.HS256)) match {
                case Success(v) => (true, v)
                case Failure(e) => (false, JObject())
            }
            isSuccess shouldBe true
            
            And("the claimed passed should be the same one recieved")
            result shouldBe claim
         }
         it("should recognize an invalid token") {
            Given("a claim containing the user & id")
            val claim = JObject(("id", 1), ("user", "hades"))
            
            When("the claim is encoded using a key to generate a token")
            val token = JwtJson4s.encode(claim, "secretKey", JwtAlgorithm.HS256)
            
            Then("the token should not be able to be decodified with another key")
            val (isFailure, failure)  = JwtJson4s.decodeJson(token, "wrongKey", Seq(JwtAlgorithm.HS256)) match {
                case Success(v) => (false, null)
                case Failure(e) => (true, Failure(e))
            }
            isFailure shouldBe true
            
            And("a JwtValidationException should be the result of the try")
            assertThrows[JwtValidationException] {
                throw(failure.exception)
            }
         }
         it("should be able to add an expiration date for the token") {
            Given("an expired claim containing the user & id")
            val claim = JwtClaim().about("Mauricio").issuedNow.expiresNow
            val key = "secretKey"

            When("the claim is encoded using a key to generate a token")
            val token = JwtJson4s.encode(claim, key, JwtAlgorithm.HS256)

            Then("the decodification of the token should fail")
            val (isFailure, failure)  = JwtJson4s.decodeJson(token, key, Seq(JwtAlgorithm.HS256)) match {
                case Success(v) => (false, null)
                case Failure(e) => (true, Failure(e))
            }
            isFailure shouldBe true
            
            And("a JwtExpirationException should be thrown")
            assertThrows[JwtExpirationException] {
                throw(failure.exception)
            }
         }
      }
      describe("when using RC4 to generate the session token") {
         it("should be able to properly encrypt & decrypt") {
            Given("an predetermined key & a text message")
            val key = "SeCrEt KeY"
            val message = "Is this working properly?"

            When("the message is encrypted")
            val cypher: String = RC4.encrypt(message, key)

            Then("decrypting the cypher should give back the original message")
            RC4.decrypt(cypher, key) should equal (message)
         }
         it("should generate a valid session key using the id") (pending)
         it("should be able to recover the id from the session key") (pending)
         it("should be able to generate mutiple sessions for the same id") (pending)
         it("should be able to retrieve the same id for multiple sessions of the same user") (pending)
      }
   }
}

@Ignore
class RedisFuncionalityTest extends RedisTest {
  
  describe("Redis") {
      it("should allow a client to connect") {
         new RedisClient(redisServer.getHost, redisServer.getBindPort) should not equal (null)
      }
      it("should allow commands to be executed") {
         r.set("test", true) shouldBe true
         info("Setting arbitraty key")

         r.del("test")
      }
      it("should allow write and read") {
         Given("a element is added")
         r.set("test", false)

         Then("the key should exist")
         r.exists("test") shouldBe true

         And("the added value should exist for the given key")
         r.get("test").get shouldBe "false"

         r.del("test")
      }
      it("should allow delete") {
         Given("a value was added")
         r.set("test", false)

         And("then removed")
         r.del("test")

         Then("the key should not exist")
         r.exists("test") shouldBe false
      }
   }
}

@Ignore
class RedisUserAuthTest extends RedisTest {
  
   describe("User authentication") {
      describe("when checking for user data") {
         it("should confirm a user exists") {
            Given("a user list is added to the server")
            r.lpush("users", "hades")

            Then("the user list must exist")
            r.exists("users") shouldBe true

            And("the user should be valid")
            DBConnector.validUser("hades").get shouldBe true

            r.del("users")
         }
         it("should confirm a user does not exists") {
            Given("a user list is added to the server")
            r.lpush("users", "hades")

            Then("the user list must exist")
            r.exists("users") shouldBe true

            And("the user should not be valid")
            DBConnector.validUser("zeus").get shouldBe false

            r.del("users")
         }
         it("should confirm a password does not exist for a user") {
            Given("a user-password map is added to the server")
            r.hmset("user-passwords", Map("zeus" -> "5678"))

            Then("the user-password map should exist")
            r.exists("user-passwords") shouldBe true

            And("the user-password pair should not exist")
            DBConnector.validPassword("hades", "1234") shouldBe None

            r.del("user-passwords")
         }
         it("should confirm a password is correct for a user") {
            Given("a user-password map is added to the server")
            r.hmset("user-passwords", Map("hades" -> "1234"))

            Then("the user-password map should exist")
            r.exists("user-passwords") shouldBe true

            val result = DBConnector.validPassword("hades", "1234")

            And("the user-passwords pair should exist in the list")
            result should not be None

            And("the password for the user should be correct")
            result.get shouldBe true

            r.del("user-passwords")
         }
         it("should confirm a password is incorrect for a user") {
            Given("a user-password map is added to the server")
            r.hmset("user-passwords", Map("hades" -> "5678"))

            Then("the user-password map should exist")
            r.exists("user-passwords") shouldBe true

            val result = DBConnector.validPassword("hades", "1234")

            And("the user-passwords pair should exist in the list")
            result should not be None

            And("the password for the user should be incorrect")
            result.get shouldBe false

         r.del("user-passwords")
         }
      }

      describe("when using ScalatraForm messages") {
         it("should show a message when the user is invalid") {
            Given("a user list is added to the server")
            r.lpush("users", "hades")

            Then("the correct message should return")
            DBConnector.validateUser("zeus").get should equal ("Invalid username")

            info(""""Invalid username"""")

            r.del("users")
         }
         it("should show a message when the user password is incorrect") {
            Given("a user-password map exists in the server")
            r.hmset("user-passwords", Map("hades" -> "1234"))

            And("a user list exists in the server")
            r.lpush("users", "hades")

            And("the user is currently waiting for password validation")
            DBConnector.validateUser("hades")

            Then("the correct message should return")
            DBConnector.validatePassword("5678").get should equal ("Invalid password")

            info(""""Invalid password"""")

            r.del("user-passwords")
            r.del("users")
         }
      } 
      
      describe("when working with sessions") {
         it("should generate & save a session for a user") (pending)
         it("should generate & save multiple sessions for a user") (pending)
         it("should generate & save multiple sessions for multiple users") (pending)
         it("should expire a sessions after its expiration time has passed") (pending)
         it("should expire a sessions after a user logs out") (pending)
         it("should expire all user's sessions prompted to") (pending)
      }
   }
}
