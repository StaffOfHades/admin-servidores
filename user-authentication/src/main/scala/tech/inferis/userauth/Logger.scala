package tech.inferis.userauth

import org.slf4j.{Logger, LoggerFactory}

object Logger {
	lazy val instance = LoggerFactory.getLogger(getClass)
}