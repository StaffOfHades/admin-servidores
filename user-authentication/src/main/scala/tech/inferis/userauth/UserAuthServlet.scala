package tech.inferis.userauth

import com.redis.RedisClient

import org.scalatra._
import org.scalatra.i18n.{I18nSupport, Messages}
import org.scalatra.forms.{ Constraint, FormSupport, label, text,  mapping, required, maxlength, minlength }

import tech.inferis.userauth.model._
import tech.inferis.userauth.db._

class UserAuthServlet extends ScalatraServlet with FormSupport with I18nSupport {

	implicit val db: DB = new Redis(new RedisClient("localhost", 6379))

	val form = mapping(
		"username" -> label("User", text(required, maxlength(32), minlength(4), exists)),
		"password" -> label("Password", text(required, maxlength(64), minlength(8), valid))
	)(User.apply)

	def exists: Constraint = new Constraint() {
		override def validate(name: String, value: String, message: Messages): Option[String] =
			DBConnector.validateUser(value)
	}

	def valid: Constraint = new Constraint() {
		override def validate(name: String, value: String, message: Messages): Option[String] =
			DBConnector.validatePassword(value)
	}

	get("/:permission") {
		views.html.permissions("/" + params("permission"))
	}

	get("/adminstrate") {
		val user = DBConnector.getUsername()
		views.html.adminstrate(user)
	}

	get("/logout") {
		DBConnector.logout()
		redirect("/login")
	}

	get("/login") {
		if (DBConnector.loggedIn()) {
			redirect("/home")
		} else {
			views.html.login()
		}
	}	

	get("/home") {
		val user = DBConnector.getUsername()
		views.html.home(user)
	}

	post("/login") {
		validate(form)(
			errors => {
				BadRequest(views.html.login())
			},
			user => {
				DBConnector.loginSuccess(user)
				redirect("/home")
			}
		)
	}

	get("/") {
		views.html.hello()
	}

	before("/:permission") {
		val permission = params("permission")
		if (!DBConnector.validPermission(s"/$permission")) {
			Logger.instance.info(s"Invalid permission for: $permission")
			redirect("/login")
		}
	}

}
