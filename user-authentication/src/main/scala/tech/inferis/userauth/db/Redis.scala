package tech.inferis.userauth.db

import com.redis._

import tech.inferis.userauth.Logger
import tech.inferis.userauth.model._

class Redis(r: RedisClient) extends DB {
	
	def validUser(user: String) = {
        r.lrange("users", 0, -1) match {
			case Some(l) => {
				val users = l.flatten
				if (users.contains(user)) {
					Some(true)
				} else {
					Some(false)
				}
			}
			case None => None
		}
	}
	
	def validateUser(value: String) =
		validUser(value) match {
			case Some(bool) if bool == true => {
                r.setex("form-user", 10, value)
                None
            }
            case Some(_) => Some("Invalid username")
			case None => Some("Invalid key user in redis. Please contanct your administrator")
		}

    def validPassword(user: String, password: String) = {
        r.hmget[String, String]("user-passwords", user) match {
            case Some(map) => {
                map.get(user) match { 
                    case Some(pwd) if pwd == password => Some(true)
                    case Some(pwd) => Some(false)
                    case None => None
                }
            }
            case None => None
        }
    }
		
	def validatePassword(value: String) =
		r.get("form-user") match {
			case Some(user) => {
                validPassword(user, value) match {
                    case Some(bool) if bool == true => {
                        r.del("form-user")
                        None
                    }
                    case Some(_) => Some("Invalid password")
                    case None => Some("No password data found for user. Please contact your administrator")
                }
            }
			case None => Some("Invalid key 'form-user' in redis. Please contact your administrator")
		}
		
    def getPermissions() = {
        var permissions: List[String] = List()
        if (r.exists("user")) {
            val user: String = r.get("user").get
            val role: Option[Int] = r.hmget[String, String]("user-roles", user) match {
                case Some(map) => map.get(user) match {
                    case Some(string) => Some(string.toInt)
                    case None => None
                }
                case None => None	
            }
            var roles: List[String] = List()
            if (!role.isEmpty) {
                val roleUnpacked = role.get
                var finished = roleUnpacked <= 0
                var byte = 1
                
                while (!finished) {
                    val specificRole = roleUnpacked & byte
                    if (specificRole > 0) {
                        r.hmget[String, String]("roles", specificRole.toString) match {
                            case Some(map) => map.get(specificRole.toString) match {
                                case Some(r) => roles = r :: roles
                                case None => Unit
                            }
                            case None => Unit
                        }
                    }
                    byte = byte << 1
                    finished = byte > 4
                }
            }
            else {
                Logger.instance.warn(s"No role found for: $user")
            }
            for (specificRole <- roles) {
                r.lrange(s"$specificRole-permissions", 0, -1) match {
                    case Some(l) => {
                        permissions = permissions ::: l.flatten
                    }
                    case None => Unit
                }
            }
        }
        permissions
    }

	def validPermission(value: String) = getPermissions().contains(value)

	def loggedIn() = r.get("user") match {
		case Some(u) if u != "guest" => true
		case _ => false
	}
	
	def loginSuccess(user: User): Unit = r.setex("user", 3600, user.username)

	def getUsername() = r.get("user") match {
		case Some(u) => u
		case None => ""
	}

	def logout(): Unit = r.get("user") match {
		case Some(_) => r.del("user")
		case None => Unit
	}
	
}
