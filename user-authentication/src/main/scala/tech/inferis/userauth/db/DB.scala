package tech.inferis.userauth.db

import tech.inferis.userauth.model._

trait DB {

    def validUser(user: String): Option[Boolean]
	def validateUser(value: String): Option[String]

	def validPassword(user: String, password: String):  Option[Boolean]
	def validatePassword(value: String): Option[String]

	def getPermissions(): List[String]
	def validPermission(value: String): Boolean

	def loggedIn(): Boolean

	def loginSuccess(user: User)

	def getUsername(): String

	def logout()
	
}
