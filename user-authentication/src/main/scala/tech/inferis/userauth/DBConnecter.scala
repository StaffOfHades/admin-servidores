package tech.inferis.userauth

import tech.inferis.userauth.db._
import tech.inferis.userauth.model._

object DBConnector {

    def validUser(user: String)(implicit db: DB): Option[Boolean] =
        db.validUser(user)

	def validateUser(value: String)(implicit db: DB): Option[String] =
		db.validateUser(value)

    def validPassword(user: String, password: String)(implicit db: DB): Option[Boolean] =
        db.validPassword(user, password)
		
	def validatePassword(value: String)(implicit db: DB): Option[String] =
		db.validatePassword(value)

	def validPermission(value: String)(implicit db: DB): Boolean =
		db.validPermission(value: String)

	def loggedIn()(implicit db: DB): Boolean = db.loggedIn()
	
	def loginSuccess(user: User)(implicit db: DB) =
		db.loginSuccess(user: User)

	def getUsername()(implicit db: DB): String = db.getUsername()

	def logout()(implicit db: DB) = db.logout()

}
